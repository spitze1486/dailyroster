<?php 

// mysqli extension

// define database host (99% chance you won't need to change this value)
$db_host = 'localhost';

// define database name, user name and password
$db_name = 'dutyroster';
$db_user = 'pinak';
$db_pwd  = '123456';

// reset record set to null ($rs is used in timetable function)
$rs = null;

// connect to database
$mysqli = new mysqli($db_host, $db_user, $db_pwd, $db_name);
if ($mysqli->connect_errno) {
	print "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

// function executes SQL statement and returns result set as Array
// $key defines column number to be set as array key (needed for fetch from redips_timetable $rs[1_2) -> Array (... ) 
function sqlQuery($sql, $key = NULL) {
	global $mysqli;
	// execute query	
	$db_result = $mysqli->query($sql);
	// if db_result is null then trigger error
	if (!$db_result) {
		print "SQL failed: (" . $mysqli->errno . ") " . $mysqli->error;
		exit();
	}
	// prepare result array
	$resultSet = Array();
	// if resulted array isn't true and that is in case of select statement then open loop
	// (insert / delete / update statement will return true on success) 
	if ($db_result !== true) {
		// loop through fetched rows and prepare result set
		while ($row = $db_result->fetch_row()) {
			// if array key is defined
			if ($key !== NULL) {
				$resultSet[$row[$key]] = $row;
			}
			// array key is not needed
			else {
				$resultSet[] = $row;
			}
		}
	}
	// return result set
	return $resultSet;
}

// commit transaction
function sqlCommit() {
	global $mysqli;
	// commit transaction
	if (!$mysqli->commit()) {
		print("Transaction commit failed\n");
		exit();
	}
	// close database connection
	$mysqli->close();
}

?>