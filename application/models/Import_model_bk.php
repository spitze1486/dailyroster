<?php
/**
 * Created by PhpStorm.
 * User: PC-5
 * Date: 9/21/2017
 * Time: 6:26 AM
 */

class Import_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function importData($data)
    {

        error_reporting(E_ALL);
        $date = strstr($data[1][3], ' ');
        $startDate = str_replace('.', '-', $date);
        $date = strstr($data[1][9], ' ');
        $lastdate = str_replace('.', '-', $date);
        $week = trim($startDate) . "_" . trim($lastdate);;

        $this->db->select('id');
        $this->db->from('basic_plan');
        $query = $this->db->get();
        $result = $query->result();

        if (count($result) > 0) {
            $this->db->where('id >', 0);
            $this->db->delete('basic_plan');
        }
        $this->db->select('week');
        $this->db->from('actual_plan');
        $this->db->where('week', $week);
        $query = $this->db->get();
        $actualPlans = $query->result();
        if (count($actualPlans) > 0) {
            $this->db->where('week', $week);
            $this->db->delete('actual_plan');
        }
        unset($data[1]);
        foreach ($data as $row) {
            $record['shift_name'] = $row[1];
            $record['function_name'] = $row[2];
            $record['week'] = $week;
            $record['mon'] = $row[3];
            $record['tue'] = $row[4];
            $record['wed'] = $row[5];
            $record['thu'] = $row[6];
            $record['fri'] = $row[7];
            $record['sat'] = $row[8];
            $record['sun'] = $row[9];
            $record['vertreter_1'] = $row[10];
            $record['vertreter_2'] = $row[11];
            $this->db->insert('basic_plan', $record);
            $this->db->insert('actual_plan', $record);
        }
        return $this->db->insert_id();
    }

    public function getWeekData($week = 0)
    {
        $this->db->select('*');
        $this->db->from('actual_plan');
        if ($week != 0)
            $this->db->where('week', $week);
        $query = $this->db->get();
        $rows = $query->result();
//        echo $this->db->last_query();die;
//        echo '<pre>';
//        print_r($rows);
//        die;
        return $rows;
    }
}