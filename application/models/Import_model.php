<?php
/**
 * Created by PhpStorm.
 * User: PC-5
 * Date: 9/21/2017
 * Time: 6:26 AM
 */

class Import_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function importData($data)
    {
        error_reporting(E_ALL);

        $this->db->select('week');
        $this->db->from('basic_plan');
        $query = $this->db->get();
        $result = $query->result();


        $startDate = date('Y-m-d', time() + (1 - date('w')) * 24 * 3600);
        $endDate = date('Y-m-d', time() + (7 - date('w')) * 24 * 3600);

        $week = trim($startDate) . "_" . trim($endDate);


        if (count($result) > 0) {
            $this->db->where('id >', 0);
            $this->db->delete('basic_plan');
        }
        $this->db->select('week');
        $this->db->from('actual_plan');
        $this->db->where('week', $week);
        $query = $this->db->get();
        $actualPlans = $query->result();
        if (count($actualPlans) > 0) {
            $this->db->where('week', $week);
            $this->db->delete('actual_plan');
        }
//        unset($data[1]);
        foreach ($data as $row) {
            $record['week'] = $week;

            if (isset($row[0])) {
                $record['col1'] = $row[0];
            } else {
                $record['col1'] = "";
            }

            if (isset($row[1])) {
                $record['col2'] = $row[1];
            } else {
                $record['col2'] = "";
            }

            if (isset($row[2])) {
                $record['col3'] = $row[2];
            } else {
                $record['col3'] = "";
            }

            if (isset($row[3])) {
                $record['col4'] = $row[3];
            } else {
                $record['col4'] = "";
            }

            if (isset($row[4])) {
                $record['col5'] = $row[4];
            } else {
                $record['col5'] = "";
            }

            if (isset($row[5])) {
                $record['col6'] = $row[5];
            } else {
                $record['col6'] = "";
            }

            if (isset($row[6])) {
                $record['col7'] = $row[6];
            } else {
                $record['col7'] = "";
            }

            if (isset($row[7])) {
                $record['col8'] = $row[7];
            } else {
                $record['col8'] = "";
            }

            if (isset($row[8])) {
                $record['col9'] = $row[8];
            } else {
                $record['col9'] = "";
            }

            if (isset($row[9])) {
                $record['col10'] = $row[9];
            } else {
                $record['col10'] = "";
            }

            if (isset($row[10])) {
                $record['col11'] = $row[10];
            } else {
                $record['col11'] = "";
            }

            if (isset($row[11])) {
                $record['col12'] = $row[11];
            } else {
                $record['col12'] = "";
            }

            if (isset($row[12]) && $row[12] != '') {
                $status = $this->getUser($row[12]);
                if (count($status) == 0) {
                    $user['name'] = $row[12];
                    $this->db->insert('users', $user);
                }
            } else {
                $record['col13'] = "";
            }

            if (isset($row[13])) {
                $record['col14'] = $row[13];
            } else {
                $record['col14'] = "";
            }

            if (isset($row[14])) {
                $record['col15'] = $row[14];
            } else {
                $record['col15'] = "";
            }

            if (isset($row[15])) {
                $record['col16'] = $row[15];
            } else {
                $record['col16'] = "";
            }

            if (isset($row[16])) {
                $record['col17'] = $row[16];
            } else {
                $record['col17'] = "";
            }

            if (isset($row[17])) {
                $record['col18'] = $row[17];
            } else {
                $record['col18'] = "";
            }

            if (isset($row[18])) {
                $record['col19'] = $row[18];
            } else {
                $record['col19'] = "";
            }

            if (isset($row[19])) {
                $record['col20'] = $row[19];
            } else {
                $record['col20'] = "";
            }
            $this->db->insert('basic_plan', $record);
            $this->db->insert('actual_plan', $record);
        }
        return $this->db->insert_id();
    }

    public function getUser($name)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('name', $name);
        $query = $this->db->get();

        return $query->result();
    }

    public function getUserColor()
    {
        $this->db->select('*');
        $this->db->from('users');
        $query = $this->db->get();
        $users = $query->result();

        $userArray = array();

        foreach ($users as $user) {
            $userArray[$user->name] = $user->color_code;
        }
        return $userArray;
    }

    public function getWeekData($week = 0)
    {
        $this->db->select('*');
        $this->db->from('actual_plan');
        if ($week != 0)
            $this->db->where('week', $week);
        $query = $this->db->get();
        $rows = $query->result();
//        echo $this->db->last_query();die;

        $rowData = [];
        $count = 0;
        foreach ($rows as $week) {
            for ($i = 0; $i <= 20; $i++) {
                if ($i == 0) {
                    $key = "id";
                } elseif ($i == 1) {
                    $rowData[$count]['week'] = $week->week;
                    $key = "col" . $i;
                } else {
                    $key = "col" . $i;
                }
                $rowData[$count][$key] = $week->$key;

            }
            $count++;
        }
//        echo '<pre>';
//        print_r($rowData);
//        die;
//unset($rowData[1]['col20']);
        $flag = 0;
        for ($j = 0; $j <= 20; $j++) {
            if ($j == 0) {
                $key = "id";
            } else {
                $key = "col" . $j;
            }
            $arr = [];
            foreach ($rowData as $row) {
                $arr[] = $row[$key];
            }

            if (strlen(implode($arr)) == 0) {
                for ($k = 0; $k < count($rowData); $k++) {
                    unset($rowData[$k][$key]);
                }
            }
        }

//        for ($i = 4; $i <= 12; $i++){
//            for($j = 1; $j < count($rowData); $j++){
//                $key = "col" . $i;
//
//                if(!key_exists($rowData[$j][$key], $usersArray)){
//                    unset($rowData[$j][$key]);
//                }
//            }
//        }
//        echo '<pre>';
//        print_r($rowData);
//        die;

        return $rowData;
    }

    public function export($week = 0)
    {
        $this->db->select('col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19, col20');
        $this->db->from('actual_plan');
        if ($week != 0)
            $this->db->where('week', $week);
        $query = $this->db->get();
        $rows = $query->result();
//        echo $this->db->last_query();die;
//        echo '<pre>';
//        print_r($rows);
//        die;
        return $rows;
    }

    /*
     Array
    (
        [name] => Kristina
        [tr1] => 2
        [td1] => col4
        [name2] => Kristina
        [tr2] => 3
        [td2] => col9_3
    )
     */
    public function save($dataArr)
    {
        $this->db->select("*");
        $this->db->from('actual_plan');
        $this->db->where('col2', 'absent');
        $query = $this->db->get();
        $absents = $query->result_array();

        $this->db->select('*');
        $this->db->from('actual_plan');
        $this->db->where('id', $dataArr['tr1']);
        $query = $this->db->get();
        $val1 = $query->result_array();
        $flag = 1;
        foreach ($absents as $absent) {
            $arr = array_values($absent);
            if (in_array($val1[0]['col11'], $arr)) {
                $flag = 0;
                break;
            }
        }
        if ($flag == 0) {
            $flag = 1;
            foreach ($absents as $absent) {
                $arr = array_values($absent);
                if (in_array($val1[0]['col12'], $arr)) {
                    $flag = 0;
                    break;
                }
            }
            if ($flag == 1) {
                $newV = $val1[0]['col12'];
            } else {
                $newV = '';
            }
        } else {
            $newV = $val1[0]['col11'];
        }
        $this->db->select($dataArr['td2']);
        $this->db->from('actual_plan');
        $this->db->where('id', $dataArr['tr2']);
        $query = $this->db->get();
        $val2 = $query->result_array();
        $newData[$dataArr['td1']] = $newV;

        $this->db->where('id', $dataArr['tr1']);
        $this->db->update('actual_plan', $newData);
//        echo $this->db->last_query() . "<br>";
        $newData = array();
        $newData[$dataArr['td2']] = $val1[0][$dataArr['td1']];
        $this->db->where('id', $dataArr['tr2']);
        $this->db->update('actual_plan', $newData);
//        echo $this->db->last_query() . "<br>";
        return true;
    }

}