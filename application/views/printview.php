<?php
$keys = array_keys($rowData[0]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Week Schedule</title>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
    <link href="<?php echo base_url(); ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>dist/css/sb-admin-2.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css" media="screen"/>
    <link href="<?php echo base_url(); ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

        <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                <span class="sr-only">Toggle navigation</span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

            </button>

            <a class="navbar-brand" href="#"></a>

        </div>

        <!-- /.navbar-header -->


        <!-- /.navbar-top-links -->


        <div class="navbar-default sidebar" role="navigation">

            <div class="sidebar-nav navbar-collapse">

                <ul class="nav" id="side-menu">

                    <li class="sidebar-search">

                        <div class="input-group custom-search-form">

                            <input type="text" class="form-control" placeholder="Search...">

                            <span class="input-group-btn">

                                <button class="btn btn-default" type="button">

                                    <i class="fa fa-search"></i>

                                </button>

                            </span>

                        </div>

                        <!-- /input-group -->

                    </li>

                    <li>

                        <img src="<?php echo base_url(); ?>images/a1.png"> Name Here


                    </li>

                    <li>

                        <img src="<?php echo base_url(); ?>images/a1.png"> Name Here


                    </li>

                    <li>

                        <img src="<?php echo base_url(); ?>images/a1.png"> Name Here


                    </li>

                    <li>

                        <img src="<?php echo base_url(); ?>images/a1.png"> Name Here


                    </li>

                    <li>

                        <img src="<?php echo base_url(); ?>images/a1.png"> Name Here


                    </li>

                    <li>

                        <img src="<?php echo base_url(); ?>images/a1.png"> Name Here


                    </li>


                </ul>

            </div>

            <!-- /.sidebar-collapse -->

        </div>

        <!-- /.navbar-static-side -->

    </nav>


    <div id="page-wrapper">


        <!-- /.row -->

        <div class="row">

            <div class="col-lg-12">

                <div id="redips-drag">


                    <!-- /.panel-heading -->

                    <div class="panel-body">

                        <table width="100%" class="table table-striped table-bordered table-hover">
                            <tbody>

                            <?php
                            //        unset($weekData[0]);

                            foreach ($rowData as $data) {
                                ?>
                                <tr id="<?php echo $data['id']; ?>">
                                    <?php
                                    for ($i = 0; $i < count($keys); $i++) {
                                        $key = $keys[$i];
                                        if ($key != "id" && $data[$key] != "") {
                                            ?>
                                            <td id="<?php echo $key; ?>">
                                                <div id="d1" class="redips-drag t1 cl"
                                                     style="background-color :
                                                     <?php if (isset($user[$data[$key]]) && $user[$data[$key]] != '') {
                                                         echo $user[$data[$key]];
                                                     } else {
                                                         echo '#fff';
                                                     } ?> !important;"><?php echo $data[$key] . "  " ?></div>
                                            </td>
                                            <?php
                                        } else {
                                            if ($key != "id") {
                                                ?>
                                                <td id="<?php echo $key; ?>"></td>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </tr>
                                <?php
                                $count++;
                            } ?>
                            </tbody>
                        </table>

                        <!-- /.table-responsive -->


                        <table id="table3" style="display:none">

                            <colgroup>
                                <col width="100"/>
                                <col width="100"/>
                                <col width="100"/>
                                <col width="100"/>
                                <col width="100"/>
                            </colgroup>

                            <tr style="">

                                <td id="message" class="redips-mark" title="You can not drop here">Table3</td>

                                <!-- jump to smile image -->

                                <td>
                                    <div id="link1" class="redips-drag t3"><a href="#smile_img"
                                                                              title="Jump to the smile image (links can be used as well)">Smile</a>
                                    </div>
                                </td>

                                <td></td>

                                <td></td>

                                <td>
                                    <div id="d16" class="redips-drag t3"><input type="checkbox" name="cb1"/><input
                                            type="checkbox" name="cb2"/><input type="checkbox" name="cb3"/></div>
                                </td>

                            </tr>


                        </table>


                    </div>

                    <!-- /.panel-body -->

                </div>

                <!-- /.panel -->

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->


        <!-- /.row -->


        <!-- /.row -->


        <!-- /.row -->

    </div>

    <!-- /#page-wrapper -->


</div>

<!-- /#wrapper -->


<!-- jQuery -->





<!-- Page-Level Demo Scripts - Tables - Use for reference -->

<script>

    $(document).ready(function () {

        $('#dataTables-example').DataTable({

            responsive: true

        });

    });

</script>


</body>


</html>

