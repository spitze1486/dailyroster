<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    var unavailableDates = [
        new Date(2012, 1, 20).valueOf(),
        new Date(2012, 1, 27).valueOf()
    ];

    function unavailable(date) {
        if (date.getDay() === 1 && $.inArray(date.valueOf(), unavailableDates) < 0) {
            return [true, ""];
        } else {
            return [false, "", "Unavailable"];
        }
    }
    $(function () {
        $('#xls').on('change', function () {
            $('#import').submit();
        });
        $("#txtDate").datepicker({
//            startDate: new Date(),
            daysOfWeekDisabled: "0,2,3,4,5,6",
            showOn: 'button',
            buttonText: 'Show Date',
            buttonImageOnly: true,
            buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif'
        });
        $('#txtDate').on('change', function () {
            $('#week').submit();
        });

    });
</script>
<div class="box-body">
    <div class="col-xs-12">
        <?php if ($this->session->flashdata('success') != '') { ?>
            <div class="alert alert-success">
                <strong><?php echo $this->session->flashdata('success'); ?></strong>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error') != '') { ?>
            <div class="alert alert-danger">
                <strong><?php echo $this->session->flashdata('error'); ?></strong>
            </div>
        <?php } ?>
    </div>
</div>
<?php
$attributes = array('id' => 'import');
echo form_open_multipart("home/import", $attributes);
?>

<div class="container-fluid">
    <ul>
        <li class="fileUpload btn btn-primary">
            <span>Import</span>
            <!--            <input type="file" class="upload" />-->
            <?php
            $arr = array(
                'name' => 'xls',
                'id' => 'xls',
                'class' => 'upload',
            );
            echo form_upload($arr);
            echo form_close();
            ?>
        </li>
        <li class="btn btn-primary pull-right">
            <form id="week" action="<?php echo base_url();?>home/week" method="post">
                <span>Week</span>
                <input type='text' name="weekdate" class="upload" id='txtDate'/>
            </form>
        </li>

</div>


<!-- Footer -->


<!-- Scripts -->
</body>
</html>