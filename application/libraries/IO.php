<?php
/**
 * Created by PhpStorm.
 * User: PC-5
 * Date: 9/21/2017
 * Time: 2:11 PM
 */
//load our new PHPExcel library
if (!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_STRICT);
require_once APPPATH . "/third_party/PHPExcel/Classes/PHPExcel/IOFactory.php";

class IO extends IOFactory
{
    public function __construct()
    {
        parent::__construct();
    }
}

?>