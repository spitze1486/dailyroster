<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Home extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
//        $this->load->library('excel_reader');
        $this->load->library('Excel');
        $this->load->library('IOFactory');
        $this->load->model('import_model', '', TRUE);
        $this->load->database();
        $this->load->helper('download');

    }

    public function index()
    {
        $this->load->view('home');
    }

    public function import()
    {
        $new_name = time() . $_FILES["xls"]['name'];
        $config = array(
            'file_name' => $new_name,
            'upload_path' => FCPATH . 'uploads/',
            'allowed_types' => '*'
        );

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('xls')) {
            $data = $this->upload->data();

            set_include_path(get_include_path() . PATH_SEPARATOR . 'uploads/');
            @chmod($data['full_path'], 0777);

            $file = $data['full_path'];
//            require_once 'PHPExcel/Classes/PHPExcel.php';

            $objPHPExcel = PHPExcel_IOFactory::load(FCPATH . 'uploads/' . $new_name);

            $data = array();

            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $worksheetTitle = $worksheet->getTitle();
                $highestRow = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

                for ($row = 1; $row <= $highestRow; ++$row) {
                    for ($col = 0; $col < $highestColumnIndex; ++$col) {
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        $val = $cell->getValue();
                        $data[$row][$col] = $val;
                    }
                }
            }
            $data = $this->import_model->importData($data);

            if ($data > 0) {
                $this->session->set_flashdata('success', 'Duty roster imported successfully');
            } else {
                $this->session->set_flashdata('error', 'Somethig was wrong in importing Duty roster');
            }
            redirect('home/view', 'refresh');

        } else {
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect('home/index', 'refresh');
        }
    }

    public function week()
    {
        $startDate = date('Y-m-d', strtotime($_POST['weekdate']));
        $endDate = date('Y-m-d', strtotime('next sunday', strtotime($startDate)));
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        $week = trim($startDate) . '_' . trim($endDate);

        $data['rowData'] = $this->import_model->getWeekData($week);
        $data['week'] = $week;

        $this->load->view('weekview', $data);
    }

    public function view()
    {
        $data['rowData'] = $this->import_model->getWeekData();
        $data['user'] = $this->import_model->getUserColor();
        $this->load->view('view', $data);
    }

    public function export()
    {
        $object = new Excel();
        $object->setActiveSheetIndex(0);

//        $table_column = array("col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8", "col9", "col10", "col11", "col12", "col13", "col14", "col15", "col16", "col17", "col18", "col19", "col20",);
        $column = 0;


        if (!isset($_POST['week'])) {
            $data = $this->import_model->getWeekData();
        } else {
            $data = $this->import_model->getWeekData($_POST['week']);
        }
        $excel_row = 1;
        $keys = array_keys($data[0]);
//        foreach ($keys as $key){
//            $table_column[] = $key;
//        }
//
//        foreach ($table_column as $field) {
//            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
//            $column++;
//        }
        foreach ($data as $row) {
            for ($i = 0; $i < count($keys); $i++) {
                $key = $keys[$i];
                $object->getActiveSheet()->setCellValueByColumnAndRow($i, $excel_row, $row[$key]);
            }
            $excel_row++;
        }

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-type: application/vnd.ms-excel');
        $name = time() . "DailyRoster.xls";
        header('Content-Disposition: attachment;filename=' . $name);
        $object_writer->save('php://output');
    }

    public function printView()
    {
//        echo 'call';die;
        if (isset($_POST['week'])) {
            $data['rowData'] = $this->import_model->getWeekData($_POST['week']);
        } else {
            $data['rowData'] = $this->import_model->getWeekData();
        }
        $data['user'] = $this->import_model->getUserColor();

        $this->load->view('printview', $data);
    }

    public function printData()
    {
        set_include_path(get_include_path() . PATH_SEPARATOR . 'uploads/printData');
        $actualName = time() . 'roster.html';
        $fileName = FCPATH . "uploads/printData/" . $actualName;
        $file = file_get_contents("http://spitzetechnology.com/dailyroster/home/printView");
//        file_put_contents($fileName, $file);

        force_download($actualName, $file);
    }

    public function save()
    {
        $this->import_model->save($_POST);
        return true;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */