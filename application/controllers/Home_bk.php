<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Home extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
//        $this->load->library('excel_reader');
        $this->load->library('Excel');
        $this->load->library('IOFactory');
        $this->load->model('import_model', '', TRUE);
        $this->load->database();

    }

    public function index()
    {
        $this->load->view('home');
    }

    public function import()
    {
        $new_name = time() . $_FILES["xls"]['name'];
        $config = array(
            'file_name' => $new_name,
            'upload_path' => FCPATH . 'uploads/',
            'allowed_types' => '*'
        );

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('xls')) {
            $data = $this->upload->data();

            set_include_path(get_include_path() . PATH_SEPARATOR . 'uploads/');
            @chmod($data['full_path'], 0777);

            $file = $data['full_path'];
//            require_once 'PHPExcel/Classes/PHPExcel.php';

            $objPHPExcel = PHPExcel_IOFactory::load(FCPATH . 'uploads/' . $new_name);

            $data = array();

            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $worksheetTitle = $worksheet->getTitle();
                $highestRow = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

                for ($row = 1; $row <= $highestRow; ++$row) {
                    for ($col = 0; $col < $highestColumnIndex; ++$col) {
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        $val = $cell->getValue();
                        $data[$row][$col] = $val;
                    }
                }
            }
            $data = $this->import_model->importData($data);

            if ($data > 0) {
                $this->session->set_flashdata('success', 'Duty roster imported successfully');
            } else {
                $this->session->set_flashdata('error', 'Somethig was wrong in importing Duty roster');
            }
            redirect('home/view', 'refresh');

        } else {
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect('home/index', 'refresh');
        }
    }

    public function week()
    {
        $startDate = date('d-m-Y', strtotime($_POST['weekdate']));
        $endDate = date('d-m-Y', strtotime('next sunday', strtotime($startDate)));
        $startDate = date('d-m-y', strtotime($startDate));
        $endDate = date('d-m-y', strtotime($endDate));
        $week = $startDate . '_' . $endDate;

        $data['weekData'] = $this->import_model->getWeekData($week);
        $data['week'] = $week;

        $this->load->view('weekview', $data);
    }

    public function view()
    {
        $data['weekData'] = $this->import_model->getWeekData();

        $this->load->view('view', $data);
    }

    public function export()
    {
        $object = new Excel();
        $object->setActiveSheetIndex(0);

        $table_column = array(" ", " ", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sat", "1.Vertreter", "2.Vertreter");
        $column = 0;

        foreach ($table_column as $field) {
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        if (!isset($_POST['week'])) {
            $data = $this->import_model->getWeekData();
        }else{
            $data = $this->import_model->getWeekData($_POST['week']);
        }
        $excel_row = 2;

        foreach ($data as $row){
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->shift_name);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->function_name);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->mon);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->tue);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->wed);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->thu);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->fri);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->sat);
            $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->sun);
            $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->vertreter_1);
            $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->vertreter_2);
            $excel_row++;
        }
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="DailyRoster.xls"');
        $object_writer->save('php://output');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */